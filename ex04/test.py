#!/usr/bin/env python3
#coding : utf-8
import main
import unittest

class TestMain(unittest.TestCase):
	def test_main(self):
		self.assertEqual(main.main("test;nok"), "Bad Input") # mauvais format
		self.assertEqual(main.main("2.5;2"), "Augmenter")
		self.assertEqual(main.main("2.6;7.530"), "KO")
		self.assertEqual(main.main("1;6"), "OK")
		self.assertEqual(main.main("1;9"), "Diminuer") 
		self.assertEqual(main.main("12,55"), "Bad Input") # mauvais format
		self.assertEqual(main.main("test;520"), "Bad Input") # mauvais format
		
if __name__ == '__main__':
	unittest.main()
