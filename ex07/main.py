#!/usr/bin/env python3
#coding : utf-8

import os
import sys
import re

debug = 0 # 1 pour le mode débug

def isInteger(var):
	"""
		La fonction isInteger(var) sert à vérifier si var est bien un int
		:param var: la variable à traiter
		:type var: int
		:return: True si var est du type attendu, sinon False
		:rtype: boolean
	"""
	if isinstance(var, int):
		return True
	else: return False


def isString(var):
	"""
		La fonction isString(var) sert à vérifier si var est bien un str
		:param var: la variable à traiter
		:type var: int
		:return: True si var est du type attendu, sinon False
		:rtype: boolean
	"""
	if isinstance(var, str):
		return True
	else: return False


def maFonction(x):
	"""
		La fonction maFonction(x) sert à traiter la valeur x
		:param x: la variable à traiter
		:type x: int
		:return: Valeur de f(x)
		:rtype: int
	"""
	#x="hello"
	#x=5
	if isInteger(x):
		valeur = int(2 * (x**3) + x - 5)
		return(valeur)
	else : raise Exception("maFonction(x): Erreur de type en entrée", x)

def procedure(fonction, borneInf, borneSup, nbPas):
	"""
		La fonction procedure(fonction, borneInf, borneSup, nbPas) sert à boucler la fonction
		:param fonction: la fonction à executer
		:param borneInf: la borne inférieure
		:param borneSup: la borne supérieure
		:param nbPas: la taille du pas
		:type fonction: str
		:type borneInf: int
		:type borneSup: int
		:type nbPas: int
		:return: Tableau de valeurs
		:rtype: array
	"""
	if isString(fonction):
		if (fonction == "maFonction"):
			if isInteger(borneInf):
				if isInteger(borneSup):
					if (borneInf < borneSup):
						if isInteger(nbPas):
							if (nbPas >= 1):
								########## PAYLOAD ###########
								tableau = []
								for i in range(borneInf, borneSup+1, nbPas):
									tableau.append(eval(fonction)(i))
								return(tableau)
								##############################
							else: raise Exception("procedure(): nbPas < 1 : ", nbPas)
						else: raise Exception("procedure(): Erreur type ou valeur : nbPas")
					else: raise Exception('procedure(): Erreur borneInf >= borneSup :', borneInf , borneSup)
				else: raise Exception("procedure(): Erreur type ou valeur : borneSup")
			else: raise Exception("procedure(): Erreur type ou valeur : borneInf")
		else: raise Exception("procedure(): Fonction '" + fonction + "' interdite")
	else: raise Exception("procedure(): Erreur type : fonction")

		
	



def main(ligne):
	"""
		La fonction main(ligne) sert à parser la ligne lue pour l'injecter dans la fonction procedure()
		:param ligne: La ligne contenant la fonction à appeler, les bornes dans lesquelles executer la fonction, la taille du pas
		:type ligne: str
		:return: Tableau de valeurs
		:rtype: array
	"""
	try:
		regex = re.compile("^([A-Za-z]+)\;(\-?[0-9]+)\;(\-?[0-9]+)\;([0-9]+)$") # initialisation de la regex
		if regex.match(ligne): # test de la regex 
			res_regex = regex.match(ligne) # récupération du résultat de la regex
			fonction = str(res_regex.group(1))
			borneInf = int(res_regex.group(2))
			borneSup = int(res_regex.group(3))
			nbPas = int(res_regex.group(4))
			return(procedure(fonction, borneInf, borneSup, nbPas))
		else: raise Exception("Regex ne catche pas, ligne=" + ligne)
	except Exception as e:
		if debug == 0 :
			return("Bad Input")
		else:
			return(e)

if __name__ == "__main__" : # si le .py est lancé en CLI
	if len(sys.argv) == 2 : # si l'argument passé à l'execution n'est pas vide
		input = sys.argv[1] # on définit le fichier à ouvrir (le premier argument)
	elif len(sys.argv) > 2 : # s'il y a plusieurs arguments
		print("Merci de spécifier le fichier INPUT en argument unique")
		exit(1) 
	else : # s'il n'y a pas d'argument
		print("Merci de spécifier le fichier INPUT en argument")
		exit(1)

	try :
		f = open(input, 'r')
		for ligne in f :
			print(main(ligne))
		f.close()
	except FileNotFoundError : 
		print("Le fichier", input, "n'existe pas")
		exit(1)
	except IOError :
		print("Problème de lecture du fichier", input)
		exit(1)
