#!/usr/bin/env python3
#coding : utf-8
import main
import unittest

class TestMain(unittest.TestCase):
	def test_main(self):
		self.assertEqual(main.main("maFonction;-5;11;2"), [-260, -62, -8, -2, 52, 250, 688, 1462, 2668])
		self.assertEqual(main.main("maFonction;-5;9;2"), [-260, -62, -8, -2, 52, 250, 688, 1462])
		self.assertEqual(main.main("maFonction;-5;9;-2"), "Bad Input")
		self.assertEqual(main.main("maFonction;-5;0;2"), [-260, -62, -8])
		self.assertEqual(main.main("maFonction;-5;-5;2"), "Bad Input")
		self.assertEqual(main.main("main;-5;-5;2"), "Bad Input")
		self.assertEqual(main.main("maFonctionBoguée;-5;-50;2"), "Bad Input")
		self.assertEqual(main.main("testàlacon"), "Bad Input")
		self.assertEqual(main.main("Azerty 🙃;-5;-1;2"), "Bad Input")

class TestisInteger(unittest.TestCase):
	def test_isInteger(self):
		self.assertEqual(main.isInteger(1), True)
		self.assertEqual(main.isInteger(.5), False)

class TestisString(unittest.TestCase):
	def test_isString(self):
		self.assertEqual(main.isString(1), False)
		self.assertEqual(main.isString(.5), False)
		self.assertEqual(main.isString("Ok Boomer"), True)

		
if __name__ == '__main__':
	unittest.main()
