#!/usr/bin/env python3
#coding : utf-8
import main
import unittest

class TestMain(unittest.TestCase):
	def test_main(self):
		self.assertEqual(main.main("4;5"), ((0.0, 0.0), (4.0, 5.0)))
		self.assertEqual(main.main("0;1"), ((0.0, 0.0), (0.0, 1.0)))
		self.assertEqual(main.main("bonsoir"), "Bad Input")
		self.assertEqual(main.main(";1"), "Bad Input")

class TestisFloat(unittest.TestCase):
	def test_isFloat(self):
		self.assertEqual(main.isFloat(1), False)
		self.assertEqual(main.isFloat(.5), True)

		
if __name__ == '__main__':
	unittest.main()
