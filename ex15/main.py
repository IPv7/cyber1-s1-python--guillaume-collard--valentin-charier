#!/usr/bin/env python3
#coding : utf-8

import os
import sys
import re

debug = 0 # 1 pour le mode débug

def isFloat(var):
	"""
		La fonction isFloat(var) sert à vérifier si var est bien un float
		:param var: la variable à traiter
		:type var: int
		:return: True si var est du type attendu, sinon False
		:rtype: boolean
	"""
	if isinstance(var, float):
		return True
	else: return False


class Vecteur2D():
	"""
		Vecteur2D() est la classe demandée dans le cadre de l'exercice
	"""

	def __init__(self, valX=None, valY=None):
		"""
			Constructeur de classe
			S'il n'y a pas de paramètres, le retour est (0;0).
		"""
		if valX == None and valY == None:
			valX, valY = float(0), float(0)
		elif valX == None or valY == None:
			raise Exception("Merci de spécifier 2 arguments")
		
		if isFloat(valX) and isFloat(valY) :
			self.x = valX
			self.y = valY
		else : raise Exception("Erreur d'instanciation de l'objet : problème de type en entrée")

	def affiche(self):
		"""
			:return: Retourne x et z
			:rtype: (float,float)
		"""
		return(round(self.x, 8), round(self.y, 8))


def main(ligne):
	"""
		La fonction main(ligne) avant instanciation de la classe
		:param ligne: La ligne contenant la fonction à appeler, les bornes dans lesquelles executer la fonction
		:type ligne: str
		:return: Tuple contenant les 2 tuple correspondant chacun à un objet de la classe : celui par défaut et celui qu'on a défini
		:rtype: tuple
	"""
	try:
		regex = re.compile("^(\-?[0-9]*\.?[0-9]+)\;(\-?[0-9]*\.?[0-9]+)$") # initialisation de la regex
		if regex.match(ligne): # test de la regex 
			res_regex = regex.match(ligne) # récupération du résultat de la regex
			x = float(res_regex.group(1))
			y = float(res_regex.group(2))
			v1 = Vecteur2D()
			v2 = Vecteur2D(x, y)
			return(v1.affiche(), v2.affiche())
		else: raise Exception("Regex ne catche pas, ligne=" + ligne)
	except Exception as e:
		if debug == 0 :
			return("Bad Input")
		else:
			return(e)

if __name__ == "__main__" : # si le .py est lancé en CLI
	if len(sys.argv) == 2 : # si l'argument passé à l'execution n'est pas vide
		input = sys.argv[1] # on définit le fichier à ouvrir (le premier argument)
	elif len(sys.argv) > 2 : # s'il y a plusieurs arguments
		print("Merci de spécifier le fichier INPUT en argument unique")
		exit(1) 
	else : # s'il n'y a pas d'argument
		print("Merci de spécifier le fichier INPUT en argument")
		exit(1)

	try :
		f = open(input, 'r')
		for ligne in f :
			print(main(ligne))
		f.close()
	except FileNotFoundError : 
		print("Le fichier", input, "n'existe pas")
		exit(1)
	except IOError :
		print("Problème de lecture du fichier", input)
		exit(1)
