#!/usr/bin/env python3
#coding : utf-8
import main
import unittest

class TestMain(unittest.TestCase):
	def test_main(self):
		self.assertEqual(main.main("8;22"), 2.75)
		self.assertEqual(main.main(".2;5"), 25.0)
		self.assertEqual(main.main("10.4;6"), 0.57692308)
		self.assertEqual(main.main("bonjour;bons01r"), "Bad Input") # mauvais format
		self.assertEqual(main.main("1533"), "Bad Input") # mauvais format
		self.assertEqual(main.main("12,55"), "Bad Input") # mauvais format
		self.assertEqual(main.main("8;0"), 0) # vitesse à 0 m/s
		self.assertEqual(main.main("0;3"), "Bad Input") # impossible de parcourir une distance en 0 seconde
		
if __name__ == '__main__':
	unittest.main()
