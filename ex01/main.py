#!/usr/bin/env python3
#coding : utf-8

import os
import re
import sys

debug = 0

def main(ligne):
	"""
		La fonction main(ligne) sert à calculer la vitesse à partir du temps et de la vitesse contenus dans la ligne
		:param ligne: La ligne contenant les valeurs temps en secondes et distance en mètres (unités SI)
		:type ligne: str
		:return: Vitesse calculée en mètre/seconde
		:rtype: float
	"""
	try:
		regex = re.compile("^([0-9]*\.?[0-9]+)\;([0-9]*\.?[0-9]+)$") # initialisation de la regex
		if regex.match(ligne) : # test de la regex 
			res_regex = regex.match(ligne) # récupération du résultat de la regex
			temps = float(res_regex.group(1))
			distance = float(res_regex.group(2))
			if temps > 0:
				vitesse = distance / temps  # calcul de la vitesse en utilisant les deux valeurs fournies par l'utilisateur
				vitesse = round(vitesse, 8) # calcul de l'arrondi de la vitesse à 8 chiffres après la virgule
				return(vitesse) # affichage du résultat
			else : raise Exception("La durée est à 0 secondes") # si la durée est à 0 secondes
		else: raise Exception("Regex ne catche pas, ligne=" + ligne)
	except Exception as e:
		if debug == 0 :
			return("Bad Input")
		else:
			return(e)

if __name__ == "__main__" : # si le .py est lancé en CLI
	if len(sys.argv) == 2 : # si l'argument passé à l'execution n'est pas vide
		input = sys.argv[1] # on définit le fichier à ouvrir (le premier argument)
	elif len(sys.argv) > 2 : # s'il y a plusieurs arguments
		print("Merci de spécifier le fichier INPUT en argument unique")
		exit(1) 
	else : # s'il n'y a pas d'argument
		print("Merci de spécifier le fichier INPUT en argument")
		exit(1)

	try :
		f = open(input, 'r')
		for ligne in f :
			print(main(ligne))
		f.close()
	except FileNotFoundError : 
		print("Le fichier", input, "n'existe pas")
		exit(1)
	except IOError :
		print("Problème de lecture du fichier", input)
		exit(1)
