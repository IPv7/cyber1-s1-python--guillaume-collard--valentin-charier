#!/usr/bin/env python3
#coding : utf-8
import main
import unittest

class TestMain(unittest.TestCase):
	def test_main(self):
		self.assertEqual(main.main(), (
			"100km/h -> 10:42\n"
			"110km/h -> 10:32\n"
			"120km/h -> 10:25\n"
			"130km/h -> 10:18\n"
			"140km/h -> 10:12\n"
			"150km/h -> 10:08\n"
			"160km/h -> 10:03\n"
			"170km/h -> 10:00\n"
			"180km/h -> 09:56\n"
			"190km/h -> 09:53\n"
			"200km/h -> 09:51\n"
			"210km/h -> 09:48\n"
			"220km/h -> 09:46\n"
			"230km/h -> 09:44\n"
			"240km/h -> 09:42\n"
			"250km/h -> 09:40\n"
			"260km/h -> 09:39\n"
			"270km/h -> 09:37\n"
			"280km/h -> 09:36\n"
			"290km/h -> 09:35\n"
			"300km/h -> 09:34"))
		
if __name__ == '__main__':
	unittest.main()
