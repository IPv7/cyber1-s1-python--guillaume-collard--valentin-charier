#!/usr/bin/env python3
#coding : utf-8

from datetime import datetime, timedelta

def isInteger(var):
	"""
		La fonction isInteger(x) sert à vérifier si x est bien un int
		:param var: la variable à traiter
		:type var: int
		:return: True si x est du type attendu, sinon False
		:rtype: boolean
	"""
	if isinstance(var, int):
		return True
	else: return False

def isString(var):
	"""
		La fonction isString(x) sert à vérifier si x est bien un str
		:param var: la variable à traiter
		:type var: int
		:return: True si x est du type attendu, sinon False
		:rtype: boolean
	"""
	if isinstance(var, str):
		return True
	else: return False


def additionnerSecondes(ajout_secondes):
	"""
		La fonction additionnerSecondes(ajout_secondes) sert à ajouter des secondes à l'heure d'origine qui est 9h00
		:param ajout_secondes: le nombre de secondes à ajouter
		:type ajout_secondes: int
		:return: Heure résultat
		:rtype: str
	"""
	if isInteger(ajout_secondes):
		if ajout_secondes > 0:
			heure_depart = 9
			minute_depart = 0
			seconde_depart = 0
			debut = datetime(100, 1, 1, heure_depart, minute_depart, seconde_depart)
			fin = debut + timedelta(seconds=ajout_secondes)
			resultat = str(fin.strftime('%H:%M'))
			return(resultat)
		else : raise Exception("additionnerSecondes(ajout_secondes): erreur de valeur de ajout_secondes", ajout_secondes)
	else : raise Exception("additionnerSecondes(ajout_secondes): Erreur de type en entrée", ajout_secondes)
	

def tchacatchac(vitesse):
	"""
		La fonction tchacatchac(vitesse) permet de renvoyer l'heure de l'impact à partir d'une vitesse moyenne 
		:param vitesse: la vitesse du train en km/h
		:type vitesse: int
		:return: Heure du drame
		:rtype: str
	"""
	if isInteger(vitesse):
		if vitesse > 0:
			vitesseEnKmParSeconde = vitesse / 3600
			timingImpactenSecondes = int(170 / vitesseEnKmParSeconde)
			heureDuDrame = additionnerSecondes(timingImpactenSecondes)
			return(heureDuDrame)
		else : raise Exception("tchacatchac(vitesse): erreur de valeur de vitesse", vitesse)
	else : raise Exception("tchacatchac(vitesse): Erreur de type en entrée", vitesse)


def main():
	"""
		La fonction main() sert à renvoyer les potentielles heures de mort, correspondantes à la vitesse 
		:return: Tableau permettant de connaître l’heure à laquelle je serai déchiqueté par le train
		:rtype: str
	"""
	try:
		sortie=""
		for i in range(100, 300+1, 10) :
			sortie += str(i) + "km/h -> " + tchacatchac(i)
			if i != 300:
				sortie += '\n' #crade mais pour avoir une sortie propre sans \n inutile à la fin
		return(sortie)
	except Exception as e:
		return(e)

if __name__ == "__main__" : # si le .py est lancé en CLI
	print(main())
