#!/usr/bin/env python3
#coding : utf-8
import main
import unittest

class TestMain(unittest.TestCase):
	def test_main(self):
		self.assertEqual(main.main("4"), 2)
		self.assertEqual(main.main(".3"), 0.54772256)
		self.assertEqual(main.main("3."), 1.73205081)
		self.assertEqual(main.main("2e2"), 14.14213562)
		self.assertEqual(main.main("cassoulet"), "Bad Input") # mauvais format
		self.assertEqual(main.main("-9"), "Bad Input") # negatif
		self.assertEqual(main.main("-.9"), "Bad Input") # negatif
		self.assertEqual(main.main("-9."), "Bad Input") # negatif
		
if __name__ == '__main__':
	unittest.main()
