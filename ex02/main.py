#!/usr/bin/env python3
#coding : utf-8

#import os
import sys
from math import sqrt
debug = 0

def main(ligne):
	"""
		La fonction main(ligne) sert à calculer la racine de la valeur si possible
		:param ligne: La ligne contenant la valeur dont il faut calculer la racine
		:type ligne: float
		:return: Racine de la valeur
		:rtype: float
	"""
	try: # pour tester si la valeur est bien un float
		ligne = float(ligne)
	except (ValueError, TypeError) :
		return("Bad Input")
	if ligne < 0 :
		return("Bad Input")
	else: return(round(sqrt(ligne), 8))



	try:
		ligne = float(ligne)
		if ligne < 0 :
			raise Exception("ligne < 0")
		else: return(round(sqrt(ligne), 8))
	except Exception as e:
		if debug == 0 :
			return("Bad Input")
		else:
			return(e)
	

if __name__ == "__main__" : # si le .py est lancé en CLI
	if len(sys.argv) == 2 : # si l'argument passé à l'execution n'est pas vide
		input = sys.argv[1] # on définit le fichier à ouvrir (le premier argument)
	elif len(sys.argv) > 2 : # s'il y a plusieurs arguments
		print("Merci de spécifier le fichier INPUT en argument unique")
		exit(1) 
	else : # s'il n'y a pas d'argument
		print("Merci de spécifier le fichier INPUT en argument")
		exit(1)

	try :
		f = open(input, 'r')
		for ligne in f :
			print(main(ligne))
		f.close()
	except FileNotFoundError : 
		print("Le fichier", input, "n'existe pas")
		exit(1)
	except IOError :
		print("Problème de lecture du fichier", input)
		exit(1)
