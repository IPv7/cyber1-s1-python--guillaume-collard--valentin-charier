#!/usr/bin/env python3
#coding : utf-8
import main
import unittest

class TestMain(unittest.TestCase):
	def test_main(self):
		self.assertEqual(main.main(), {'d', 'b'})
		
if __name__ == '__main__':
	unittest.main()