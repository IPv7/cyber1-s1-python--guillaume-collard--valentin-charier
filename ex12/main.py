#!/usr/bin/env python3
#coding : utf-8

def main():
    """Cette fonction réalise des test de présence 
    d'éléments dans des ensembles et affiche des
    sous-ensembles   
    Returns:
        boolean -- Retourne le boolean correspondant aux tests
    """


    X=set(["a","b","c","d"])
    Y=set(["s","b","d"])
    print(X)
    print(Y)
    test= "c" in X
    print("c in X :",test) 
    test= "a" in Y
    print("a in Y :",test)
    test=X - Y
    print("X - Y :", test)
    test=Y - X
    print("Y - X :", test)
    test=X | Y
    print("X union Y :", test)
    test=Y & X
    print("Y inter X : ", end="")
    return test



if __name__ == "__main__" : # si le .py est lancé en CLI
	print(main())