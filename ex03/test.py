#!/usr/bin/env python3
#coding : utf-8
import main
import unittest

class TestMain(unittest.TestCase):
	def test_main(self):
		self.assertEqual(main.main("cassoulet;canard"), "canard")
		self.assertEqual(main.main("c:d"), "Bad Input") # mauvais format : ":"
		self.assertEqual(main.main("giv;e"), "e")
		self.assertEqual(main.main("ba;bad"), "ba") 
		self.assertEqual(main.main("lower;greater"), "greater") 
		self.assertEqual(main.main("w0rd;incorrect"), "Bad Input") # mauvais format : chiffre
		self.assertEqual(main.main("mot;accentué"), "Bad Input") # mauvais format : mot accentué
		self.assertEqual(main.main("mot;accentue"), "accentue") 
		
if __name__ == '__main__':
	unittest.main()
