#!/usr/bin/env python3
#coding : utf-8

import os
import re
import sys

debug = 0

def main(ligne):
	"""
		La fonction main(ligne) sert à comparer 2 mots et retourner le plus "petit"
		:param ligne: La ligne contenant les 2 mots à tester
		:type ligne: str
		:return: Mot le plus petit
		:rtype: str
	"""
	try:
		# initialisation de la regex, qui accepte toutes les lettres (non accentuées), les mots à caractères spéciaux sont rejetés
		regex = re.compile("^([A-Za-z]+)\;([A-Za-z]+)$")
		if regex.match(ligne) : # test de la regex
			res_regex = regex.match(ligne) # récupération du résultat de la regex
			mot1 = res_regex.group(1).lower() # sort le mot 1 et le convertit en minuscule
			mot2 = res_regex.group(2).lower() # idem pour le 2
			if mot1 <= mot2 : # si le mot 1 est plus petit ou égal au mot 2
				return(mot1)
			else : return(mot2)
		else: raise Exception("Regex ne catche pas, ligne=" + ligne)
	except Exception as e:
		if debug == 0 :
			return("Bad Input")
		else:
			return(e)




if __name__ == "__main__" : # si le .py est lancé en CLI
	if len(sys.argv) == 2 : # si l'argument passé à l'execution n'est pas vide
		input = sys.argv[1] # on définit le fichier à ouvrir (le premier argument)
	elif len(sys.argv) > 2 : # s'il y a plusieurs arguments
		print("Merci de spécifier le fichier INPUT en argument unique")
		exit(1) 
	else : # s'il n'y a pas d'argument
		print("Merci de spécifier le fichier INPUT en argument")
		exit(1)

	try :
		f = open(input, 'r')
		for ligne in f :
			print(main(ligne))
		f.close()
	except FileNotFoundError : 
		print("Le fichier", input, "n'existe pas")
		exit(1)
	except IOError :
		print("Problème de lecture du fichier", input)
		exit(1)
