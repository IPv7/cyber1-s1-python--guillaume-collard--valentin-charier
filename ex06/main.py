#!/usr/bin/env python3
#coding : utf-8

import sys
from math import pi

debug = 0

def isFloat(var):
	"""
		La fonction isFloat(var) sert à vérifier si var est bien un float
		:param var: la variable à traiter
		:type var: float
		:return: True si var est du type attendu, sinon False
		:rtype: boolean
	"""
	if isinstance(var, float):
		return True
	else: return False

def cube(arg):
	"""
		La fonction cube(arg) sert à calculer le cube du paramètre indiqué
		:param arg: La valeur dont il faut calculer le cube
		:type ligne: float
		:return: Cube de la valeur
		:rtype: float
	"""
	return(float(arg**3))

def volumeSphere(rayon):
	"""
		La fonction volumeSphere(rayon) sert à calculer le volume d'une sphère à partir du paramètre indiqué
		:param rayon: Le rayon de la sphère à calculer
		:type ligne: float
		:return: Volume de la sphère
		:rtype: float
	"""
	volume = 4 / 3 * pi * cube(float(rayon))
	return(volume)

def main(ligne):
	"""
		La fonction main(ligne) sert à calculer le volume d'une sphère à partir du paramètre indiqué
		:param ligne: La ligne contenant la valeur initiale
		:type ligne: str
		:return: Volume de la sphère
		:rtype: float
	"""
	try:
		ligne = float(ligne)
		if ligne >= 0 :
			return(round(volumeSphere(ligne), 8))
		else: raise Exception("ligne < 0")
	except Exception as e:
		if debug == 0 :
			return("Bad Input")
		else:
			return(e)



if __name__ == "__main__" : # si le .py est lancé en CLI
	if len(sys.argv) == 2 : # si l'argument passé à l'execution n'est pas vide
		input = sys.argv[1] # on définit le fichier à ouvrir (le premier argument)
	elif len(sys.argv) > 2 : # s'il y a plusieurs arguments
		print("Merci de spécifier le fichier INPUT en argument unique")
		exit(1) 
	else : # s'il n'y a pas d'argument
		print("Merci de spécifier le fichier INPUT en argument")
		exit(1)

	try :
		f = open(input, 'r')
		for ligne in f :
			print(main(ligne))
		f.close()
	except FileNotFoundError : 
		print("Le fichier", input, "n'existe pas")
		exit(1)
	except IOError :
		print("Problème de lecture du fichier", input)
		exit(1)
