#!/usr/bin/env python3
#coding : utf-8
import main
import unittest

class TestMain(unittest.TestCase):
	def test_main(self):
		self.assertEqual(main.main("3"), 113.09733553)
		self.assertEqual(main.main("ee"), "Bad Input") # mauvais format
		self.assertEqual(main.main("556"), 719967651.9032462)
		self.assertEqual(main.main("zzz:5"), "Bad Input") # mauvais format
		self.assertEqual(main.main("8.4"), 2482.71270954)
		self.assertEqual(main.main("0"), 0.0) 

class TestCube(unittest.TestCase):
	def test_cube(self):
		self.assertEqual(main.cube(3), 27)
		self.assertEqual(main.cube(0), 0)

class TestSphere(unittest.TestCase):
	def test_sphere(self):
		self.assertEqual(main.volumeSphere(10), 4188.790204786391)
		self.assertEqual(main.volumeSphere(0), 0)
		
if __name__ == '__main__':
	unittest.main()
