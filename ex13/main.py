#!/usr/bin/env python3
# coding : utf-8

import os
import sys

debug = 0

def isString(var):
	"""
		La fonction isString(var) sert à vérifier si var est bien un str
		:param var: la variable à traiter
		:type var: int
		:return: True si var est du type attendu, sinon False
		:rtype: boolean
	"""
	if isinstance(var, str):
		return True
	else: return False

def main(michel):
	"""Cette fonction range les mots de la mots dans un tableau

	Arguments:
		michel {table[str]} -- phrase renseignée en entrée

	Returns:
		dict -- ensemble avec pour chaque mot le nombre de fois qu'il apparaît dans la mots
	"""
	try:
		if isString(michel):
			mots = []
			mots = michel.split(" ")
			mots.sort()
			return compterMots(mots)
		else: raise Exception("Erreur de type entré dans main() : ", michel)
	except Exception as e:
		if debug == 0 :
			return("Bad Input")
		else:
			return(e)
	


def compterMots(mots):
	"""cette fonction compte le nombre
	d'occurence d'un mot dans la mots donnée
	
	Arguments:
		mots {table[str]} -- tableau contenant les mots de la mots
	
	Returns:
		dict -- ensemble avec pour chaque mot le nombre de fois qu'il apparaît dans la mots
	"""
	dict_ = dict()
	for mot in mots:
		if mot in dict_:
			dict_[mot] += 1
		else:
			dict_[mot] = 1

	return dict_


if __name__ == "__main__":  # si le .py est lancé en CLI
	if len(sys.argv) == 2:  # si l'argument passé à l'execution n'est pas vide
		input = sys.argv[1]  # on définit le fichier à ouvrir (le premier argument)
	elif len(sys.argv) > 2:  # s'il y a plusieurs arguments
		print("Merci de spécifier le fichier INPUT en argument unique")
		exit(1)
	else:  # s'il n'y a pas d'argument
		print("Merci de spécifier le fichier INPUT en argument")
		exit(1)

	try:
		f = open(input, "r")
		for ligne in f:
			print(main(ligne))
		f.close()
	except FileNotFoundError:
		print("Le fichier", input, "n'existe pas")
		exit(1)
	except IOError:
		print("Problème de lecture du fichier", input)
		exit(1)
