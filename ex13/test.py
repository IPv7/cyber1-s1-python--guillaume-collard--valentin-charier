#!/usr/bin/env python3
# coding : utf-8
import main
import unittest


class TestMain(unittest.TestCase):
    def test_main(self):
        self.assertEqual(main.main("Bonjour je m'appelle Valentin et je viens de La Rochelle"),
         {'Bonjour': 1, 'La': 1, 'Rochelle': 1, 'Valentin': 1, 'de': 1, 'et': 1, 'je': 2, "m'appelle": 1, 'viens': 1})


class TestcompterMots(unittest.TestCase):
    def test_compterMots(self):
        self.assertEqual(
            main.compterMots(["Bonjour", "bonjour"]), {"Bonjour": 1, "bonjour": 1}
        )


if __name__ == "__main__":
    unittest.main()

