#!/usr/bin/env python3
#coding : utf-8

def main():
    """cette fonction ne prend pas d'argument et 
    ajoute 3 au éléments de la liste supérieurs ou égaux à 2.
    
    Returns:
        list -- liste avec les éléments supérieurs ou égaux à 2 incrémentés de 3
    """
    liste=[1, 4, 2, 1, 0, 5, 4, 3]
    res_list=[i+3 if i>=2 else i for i in liste]
    return res_list


if __name__ == "__main__" : # si le .py est lancé en CLI
	print(main())