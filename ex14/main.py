#!/usr/bin/env python3
#coding : utf-8

import os
import sys

debug = 0 # 1 pour le mode débug

class maClasse():
	"""
		maClasse() est la classe demandée dans le cadre de l'exercice
	"""
	x = 23
	y = x + 5

	def affiche(self):
		"""
			Affiche y et z
			:return: y et z
			:rtype: (int,int)
		"""
		self.z = 42
		return(self.y, self.z)

def main():
	"""
		Instance un objet de maClasse et retourne le resultat
		:return: y et z
		:rtype: (int,int)
	"""
	try:
		objet = maClasse()
		resultat = objet.affiche()
		return(resultat)
	except Exception as e:
		return(e)


if __name__ == "__main__" : # si le .py est lancé en CLI
	print(main())