#!/usr/bin/env python3
#coding : utf-8

def main():
    """Cette fonction prend la liste, la trie,
    ajoute l'élément 12, la retourne, affiche l'index de l'élément "17",
    supprime l'élément "38", affiche des sous listes.
    
    Returns:
        list -- affiche les listes et sous liste après chaque oppération effectuée sur la liste
    """
    liste=[17, 38, 10, 25, 72]
    liste.sort()
    print(liste[:])
    liste.append(12)
    print(liste[:])
    liste.reverse()
    print(liste[:])
    print(liste.index(17))
    liste.remove(38)
    print(liste[:])
    print(liste[2:3])
    print(liste[:2])
    print(liste[3:])
    print(liste[:])
    return liste[-1]



if __name__ == "__main__" : # si le .py est lancé en CLI
	print(main())