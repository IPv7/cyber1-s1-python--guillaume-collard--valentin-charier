#!/usr/bin/env python3
#coding : utf-8

def main():
    """cette fonction réalise des combinaisons 
    à partir de 2 chaines de caractères
    
    Returns:
        list -- liste des combinaisons réalisées 2 à 2 avec les chaines de caractères 
    """
    chaine1="abc"
    chaine2="de"
    res_list=[i+x for i in chaine1 for x in chaine2]
    return res_list


if __name__ == "__main__" : # si le .py est lancé en CLI
	print(main())