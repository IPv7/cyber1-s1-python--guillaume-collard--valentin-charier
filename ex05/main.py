#!/usr/bin/env python3
#coding : utf-8
def main():
	"""
		La fonction main() sert à générer un tableau d'entiers entre 0 et 15 non inclus, de 3 en 3
		:return: Tableau généré
		:rtype: array
	"""
	tableau = []
	for i in range(0, 15, 3) :
		tableau.append(i)
	return tableau

if __name__ == "__main__" : # si le .py est lancé en CLI
	print(main())