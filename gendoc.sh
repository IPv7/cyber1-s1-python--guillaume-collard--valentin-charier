for i in {01..16}
do
   cd ex$i
   pydoc -w main
   mv main.html doc.html
   rm -rf __pycache__
   cd ..
done
